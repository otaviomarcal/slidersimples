function Slider() {

    var pagination = {
        total: $('.slider li').length,
        actual: 1
    };

    var settings = {
        slideShow: {
            controls_click_restart: true,
            seconds_transactions: 3000
        }
    };

    /* variavel pro setIterval global no slider */
    var intervalId;

    return {
        start: function () {

            /* coloco um id pra cada item do banner */
            this.renameItems();

            /* crio a paginação */
            this.createPagination();

            /* inicio o banner com a pagina inicial conforme
             o objeto pagination acima, no caso do 1*/
            this.setActualPage(this.getActualPage());

            /* inicio o startSlideShow*/
            this.startSlideShow();
        },
        next: function () {

            if (this.getActualPage() == pagination.total) {
                this.setActualPage(1);
                return;
            }
            this.setActualPage(this.getActualPage() + 1);
        },
        prev: function () {
            if (this.getActualPage() == 1) {
                this.setActualPage(pagination.total);
                return;
            }
            this.setActualPage(this.getActualPage() - 1);
        },
        renameItems: function () {
            var n = 1;
            $('.slider li').each(function (item) {
                $(this).attr('id', 'slide-item-' + n);
                n++;
            })
        },
        setActualPage: function (page_number) {
            pagination.actual = page_number;
            this.hideAllItems();
            $('#slide-item-' + this.getActualPage()).addClass('active');
            $('#bolinha-' + this.getActualPage()).addClass('active');
        },
        getActualPage: function () {
            return parseInt(pagination.actual);
        },
        hideAllItems: function () {
            $('.slider li').removeClass('active');
            $('.pagination a').removeClass('active');
        },
        getTotal: function () {
            return pagination.total;
        },
        createPagination: function () {
            for (var i = 1; i <= this.getTotal(); i++) {
                $('.pagination').append('<li><a href="' + i + '" id="bolinha-' + i + '" title="" class="bt-pagination">' + (i) + '</a></li>');
            }

            this.bindControls();
        },
        bindControls: function () {

            var self = this;

            $('.pagination a').bind('click', function (e) {
                e.preventDefault();

                self.decisionSlideShow();

                var page_number = $(this).attr('href');
                self.setActualPage(page_number);
            });

            $('.bt-next').bind('click', function (e) {
                e.preventDefault();

                self.decisionSlideShow();
                self.next();
            });

            $('.bt-prev').bind('click', function (e) {
                e.preventDefault();

                self.decisionSlideShow();
                self.prev();
            });

            $('.bt-play').bind('click', function (e) {
                e.preventDefault();
                self.startSlideShow();
            });

            $('.bt-stop').bind('click', function (e) {
                e.preventDefault();
                self.stopSlideShow();
            });
        },
        startSlideShow: function () {
            var self = this;
            intervalId = setInterval(function () {
                self.next();
            }, settings.slideShow.seconds_transactions);
        },
        stopSlideShow: function () {
            clearInterval(intervalId)
        },
        restartSlideShow: function () {
            this.stopSlideShow();
            this.startSlideShow();
        },
        decisionSlideShow: function() {
            if(settings.slideShow.controls_click_restart) {
                this.restartSlideShow();
                return;
            }
            this.stopSlideShow();
        }
}
}